<?php

namespace app\controllers;

use app\models\Data;
use Yii;
use yii\web\Controller;

class SiteController extends Controller
{
    public function actionIndex()
    {
        $errors = [];
        $params = Yii::$app->request->get();

        if (!isset($params['a'])){
            $errors[] = 'Отсуствует значение "a"';
        }

        if (!isset($params['b'])){
            $errors[] = 'Отсуствует значение "a"';
        }

        if (!count($errors)){
            if ($params['a']!=='0'){
                $model = new Data();
                $model->value = $params['b'];
                $model->save();
            }
            else{
                print_r($params['b']);
            }
        }
        die;
    }

}
