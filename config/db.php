<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=mini_test',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
